const themeChange = document.getElementById("themeChange");
let clicked = false;
themeChange.addEventListener("click", () => {
    if (clicked === false) {
        document.body.style.backgroundColor = "var(--bg-light)";
        themeChange.innerHTML = "DarkMode";
        clicked = true;
    } else {
        document.body.style.backgroundColor = "var(--bg-dark)";
        themeChange.innerHTML = "LightMode";
        clicked = false;
    }
})